import Vue from 'vue'
import App from './App'
import router from './router'

import 'element-ui/lib/theme-chalk/index.css'

import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

import { Row, Col, Table, TableColumn, Button, Dialog, Form, FormItem, Input, Upload, DatePicker, Message, Notification } from 'element-ui'

locale.use(lang);
Vue.config.productionTip = false;

Vue.prototype.$ELEMENT = { size: 'small' };

Vue.use(Button);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Row);
Vue.use(Col);
Vue.use(Dialog);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Input);
Vue.use(Upload);
Vue.use(DatePicker);
Vue.use(Message);
Vue.use(Notification);

Vue.prototype.$message = Message;
Vue.prototype.$notify = Notification;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});
