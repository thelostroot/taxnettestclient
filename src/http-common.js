import axios from 'axios';

export const HTTP = axios.create({
  baseURL: '/',
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json'
  }
});
